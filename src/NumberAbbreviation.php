<?php

namespace Drupal\number_abbreviation;

/**
 * Class NumberAbbreviation.
 */
class NumberAbbreviation implements NumberAbbreviationInterface {

  /**
   * Abbreviate numbers with too many digits.
   *
   * @param int $number
   *   The number value.
   *
   * @return string
   *   the abbreviated number.
   */
  public function abbreviate($number, $decimal = 1) {
    // Make sure the number is in correct format.
    $number = ($number == (int) $number) ? (int) $number : (float) $number;

    if ($number < 1000) {
      // Anything less than a thousand.
      $number = number_format($number, $decimal);
    }
    elseif ($number < 1000000) {
      // Anything less than a million.
      $number = number_format($number / 1000, $decimal) . 'K';
    }
    elseif ($number < 1000000000) {
      // Anything less than a billion.
      $number = number_format($number / 1000000, $decimal) . 'M';
    }
    else {
      // At least a billion.
      $number = number_format($number / 1000000000, $decimal) . 'B';
    }

    return str_replace('.00', '', $number);
  }

}

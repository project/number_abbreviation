<?php

namespace Drupal\number_abbreviation\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Drupal\number_abbreviation\NumberAbbreviationInterface;

/**
 * Defines a controller for TwigExtension.
 */
class TwigExtension extends AbstractExtension {

  /**
   * The number abbreviation service.
   *
   * @var \Drupal\number_abbreviation\NumberAbbreviation
   */
  protected $number_abbreviation;

  /**
   * Constructs a new TwigExtension object.
   */
  public function __construct(NumberAbbreviationInterface $number_abbreviation) {
    $this->number_abbreviation = $number_abbreviation;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('number_abbreviate', [$this, 'numberAbbreviate']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('number_abbreviate', [$this, 'numberAbbreviate'], []),
    ];
  }

  /**
   * Abbreviate a number value.
   *
   * @param int|float $number
   *   The value to be formatted.
   *
   * @return string
   *   The formatted value.
   */
  public function numberAbbreviate($number, $decimal = 1) {
    return $this->number_abbreviation->abbreviate($number, $decimal);
  }

}

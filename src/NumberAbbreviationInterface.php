<?php

namespace Drupal\number_abbreviation;

/**
 * Provides an interface defining for NumberAbbreviation.
 */
interface NumberAbbreviationInterface {

  /**
   * Abbreviate numbers with too many digits.
   *
   * @param int $number
   *   The number value.
   *
   * @return string
   *   the abbreviated number.
   */
  public function abbreviate($number);

}

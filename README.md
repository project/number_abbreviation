# INTRODUCTION
Number Abbreviation For Drupal.

A simple module that provides the ability to abbreviate numbers with too many
digits to a shorter format (2k, 2.5k, 2m, 2.5m, etc) using a twig formatter.
It also provides a number field formatter to use with fields.
Other modules or hooks can use the service provide


# TWIG FORMATTER USAGE
if you want to use it as a filter (1 decimal point)
`{{ field__value|number_abbreviate }}`

if you want to use it as a function and have a custom number of decimal points
`{{ number_abbreviate(field__value , number_of_decimal_points) }}`

# SERVICE USAGE
`\Drupal::service('number_abbreviation.abbreviate')->abbreviate($number_to_format)`

# AUTHOR
Yasser Samman
https://jo.linkedin.com/in/yasseralsamman
y.samman@codersme.com
